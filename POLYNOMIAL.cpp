// using namespace std;
#include <iostream>
#include <varargs.h>
#include <cstdarg>

using namespace std;

int NWD(int a, int b) {
    if (a<0) a = -a;
    if (b<0) b = -b;
    while (a!=b) {
        if (a>b) {
            a-=b;
        } else {
            b-=a;
        }
    }
    return a;
}

class POLYNOMIAL {
private:
    void normalizePolynomial() {
        if (args[level]) {
            int nwd = args[level]<0?-args[level]:args[level];
            for (int i = level; i>0; i--) {
                if (!args[i-1]) {
                    continue;
                }
                nwd = NWD(nwd, args[i-1]);
            }
            for (int i = 0; i <= level; i++) {
                args[i] = args[i]/nwd;
            }
        }
    }
public:
    int* args;
    int level;
    static int overloaded;

    explicit POLYNOMIAL(int level, ...) {
        this->level = level;
        va_list arguments;
        level++;
        va_start(arguments, level);
        args = new int[level];
        for (int i = 0; i < level; i++) {
            args[i] = va_arg(arguments, int);
        }
        va_end(arguments);
        normalizePolynomial();
    }

    POLYNOMIAL() {
        level = 0;
        args = new int[1];
        args[0] = 0;
    }

    POLYNOMIAL(const POLYNOMIAL& p) {
        this->level = p.level;
        this->args = new int[this->level+1];
        for (int i = 0; i <= this->level; i++) {
            this->args[i] = p.args[i];
        }
    }

    friend ostream& operator<<(ostream&, const POLYNOMIAL&);
    friend istream& operator>>(istream&, POLYNOMIAL&);

    void* operator new(size_t amount) {
        overloaded++;
        return ::new char[amount];
    }

    void operator delete(void* pointer) {
        overloaded--;
        ::delete (POLYNOMIAL*) pointer;
    }

    POLYNOMIAL& operator=(const POLYNOMIAL& p) {
        this->level = p.level;
        delete this->args;
        this->args = new int[p.level+1];
        for (int i = 0; i <= p.level; i++) {
            this->args[i] = p.args[i];
        }
        return *this;
    }

    POLYNOMIAL operator-() {
        POLYNOMIAL tmp;
        tmp.level = level;
        tmp.args = new int[level+1];
        for (int i = 0; i <= this->level; i++) {
            tmp.args[i] = -(this->args[i]);
        }
        return tmp;
    }

    POLYNOMIAL operator+(const POLYNOMIAL& p) {
        POLYNOMIAL tmp;
        if (this->level==p.level) {
            tmp.level = p.level;
            while (this->args[tmp.level]==-(p.args[tmp.level]) && tmp.level>0) {
                tmp.level = tmp.level-1;
            }
        } else {
            tmp.level = this->level>p.level?this->level:p.level;
        }
        tmp.args = new int[tmp.level+1];
        for (int i = 0; i <= tmp.level; i++) {
            tmp.args[i] = 0;
            if (i<=this->level) {
                tmp.args[i] += this->args[i];
            }
            if (i<=p.level) {
                tmp.args[i] += p.args[i];
            }
        }
        tmp.normalizePolynomial();
        return tmp;
    }

    POLYNOMIAL operator-(const POLYNOMIAL& p) {
        POLYNOMIAL tmp;
        if (this->level==p.level) {
            tmp.level = p.level;
            while (this->args[tmp.level]==p.args[tmp.level] && tmp.level>0) {
                tmp.level = tmp.level-1;
            }
        } else {
            tmp.level = this->level>p.level?this->level:p.level;
        }
        tmp.args = new int[tmp.level+1];
        for (int i = 0; i<=tmp.level; i++) {
            tmp.args[i] = 0;
            if (i<=this->level) {
                tmp.args[i] += this->args[i];
            }
            if (i<=p.level) {
                tmp.args[i] -= p.args[i];
            }
        }
        tmp.normalizePolynomial();
        return tmp;
    }

    POLYNOMIAL operator*(const POLYNOMIAL& p) {
        POLYNOMIAL tmp;
        tmp.level = this->level+p.level;
        tmp.args = new int[tmp.level+1];
        for (int i = 0; i<=tmp.level; i++) {
            tmp.args[i] = 0;
        }
        for (int i = 0; i <= this->level; i++) {
            for (int j = 0; j <= p.level; j++) {
                tmp.args[i+j] += this->args[i]*p.args[j];
            }
        }
        while (tmp.args[tmp.level]==0 && tmp.level>0) {
            tmp.level--;
        }
        tmp.normalizePolynomial();
        return tmp;
    }

    POLYNOMIAL operator/(const POLYNOMIAL& p) {
        POLYNOMIAL tmp;
        POLYNOMIAL r = p;
        int d = this->level;
        double c = this->args[this->level];
        while (r.level >= d) {
            cout << r.level << ", " << d << endl;
            POLYNOMIAL s;
            if (s.level<r.level - d) {
                delete s.args;
                s.args = new int[r.level - d];
            }
            s.args[r.level - d] = r.args[r.level] / c;
            tmp += s;
            r -= s*(*this);
        }
        return tmp;
    }

    POLYNOMIAL operator%(const POLYNOMIAL& p) {
        POLYNOMIAL tmp;
        POLYNOMIAL r = p;
        int d = this->level;
        double c = this->args[this->level];
        while (r.level >= d) {
            cout << r.level << ", " << d << endl;
            POLYNOMIAL s;
            if (s.level<r.level - d) {
                delete s.args;
                s.args = new int[r.level - d];
            }
            s.args[r.level - d] = r.args[r.level] / c;
            tmp += s;
            r -= s*(*this);
        }
        return r;
    }

    POLYNOMIAL operator>>(const int count) {
        POLYNOMIAL tmp;
        tmp.level = level-count;
        delete tmp.args;
        if (tmp.level<0 || count<0) {
            tmp.level = 0;
            tmp.args = new int[1];
            tmp.args[0] = 0;
        } else {
            tmp.args = new int[tmp.level+1];
            for (int i = 0; i <= tmp.level; i++) {
                tmp.args[i] = this->args[i+count];
            }
        }
        tmp.normalizePolynomial();
        return tmp;
    }

    POLYNOMIAL operator<<(const int count) {
        POLYNOMIAL tmp;
        if (count<0) {
            return tmp;
        }
        if (!(this->level == 0 && this->args[0] == 0)) {
            tmp.level = level+count;
            delete tmp.args;
            tmp.args = new int[tmp.level+1];
            for (int i = tmp.level; i >= count; i--) {
                tmp.args[i] = this->args[i-count];
            }
            for (int i = 0; i < count; i++) {
                tmp.args[i] = 0;
            }
        }
        tmp.normalizePolynomial();
        return tmp;
    }

    POLYNOMIAL& operator+=(const POLYNOMIAL& p) {
        POLYNOMIAL tmp = (*this) + p;
        this->level = tmp.level;
        bool decr = true;
        for (int i = this->level; i >= 0; i--) {
            this->args[i] = tmp.args[i];
            if (decr) {
                if (this->args[i]==0 && this->level>0) {
                    this->level--;
                } else {
                    decr = false;
                }
            }
        }
        return *this;
    }

    POLYNOMIAL& operator-=(const POLYNOMIAL& p) {
        POLYNOMIAL tmp = (*this) - p;
        this->level = tmp.level;
        bool decr = true;
        for (int i = this->level; i >= 0; i--) {
            this->args[i] = tmp.args[i];
            if (decr) {
                if (this->args[i]==0 && this->level>0) {
                    this->level--;
                } else {
                    decr = false;
                }
            }
        }
        return *this;
    }

    POLYNOMIAL& operator*=(const POLYNOMIAL& p) {
        POLYNOMIAL tmp = (*this) * p;
        this->level = tmp.level;
        for (int i = 0; i <= this->level; i++) {
            this->args[i] = tmp.args[i];
        }
        return *this;
    }

    POLYNOMIAL& operator/=(const POLYNOMIAL& p) {
        POLYNOMIAL tmp = (*this) / p;
        this->level = tmp.level;
        for (int i = 0; i <= this->level; i++) {
            this->args[i] = tmp.args[i];
        }
        return *this;
    }

    POLYNOMIAL& operator%=(const POLYNOMIAL& p) {
        POLYNOMIAL tmp = (*this) % p;
        this->level = tmp.level;
        for (int i = 0; i <= this->level; i++) {
            this->args[i] = tmp.args[i];
        }
        return *this;
    }

    POLYNOMIAL& operator>>=(const int count) {
        POLYNOMIAL tmp = (*this) >> count;
        this->level = tmp.level;
        for (int i = 0; i <= this->level; i++) {
            this->args[i] = tmp.args[i];
        }
        return *this;
    }

    POLYNOMIAL& operator<<=(const int count) {
        POLYNOMIAL tmp = (*this) << count;
        this->level = tmp.level;
        for (int i = 0; i <= this->level; i++) {
            this->args[i] = tmp.args[i];
        }
        return *this;
    }

    POLYNOMIAL& operator--() {
        bool decr = true;
        for (int i = this->level; i >= 0; i--) {
            --this->args[i];
            if (decr) {
                if (this->args[i]==0 && this->level>0) {
                    this->level--;
                } else {
                    decr = false;
                }
            }
        }
        normalizePolynomial();
        return (*this);
    }

    const POLYNOMIAL operator--(int a) {
        POLYNOMIAL p = *this;
        --(*this);
        return p;
    }

    POLYNOMIAL& operator++() {
        bool incr = true;
        for (int i = this->level; i >= 0; i--) {
            this->args[i]++;
            if (incr) {
                if (this->args[i]==0 && this->level>0) {
                    this->level--;
                } else {
                    incr = false;
                }
            }
        }
        normalizePolynomial();
        return (*this);
    }

    const POLYNOMIAL operator++(int a) {
        POLYNOMIAL p = *this;
        ++(*this);
        return p;
    }

    int getLevel() {
        return level;
    }

    int* getArgs() {
        return args;
    }
};

bool operator<(POLYNOMIAL& p1, POLYNOMIAL& p2) {
    if (p1.getLevel()>p2.getLevel()) {
        return false;
    } else if (p1.getLevel()==p2.getLevel()) {
        for (int i = p2.getLevel(); i>=0; i--) {
            if (p1.getArgs()[i]<p2.getArgs()[i]) {
                return true;
            }
            if (p1.getArgs()[i]>p2.getArgs()[i]) {
                return false;
            }
        }
    }
    return false;
}

bool operator>(POLYNOMIAL& p1, POLYNOMIAL& p2) {
    if (p1.getLevel()<p2.getLevel()) {
        return false;
    } else if (p1.getLevel()==p2.getLevel()) {
        for (int i = p2.getLevel(); i>=0; i--) {
            if (p1.getArgs()[i]>p2.getArgs()[i]) {
                return true;
            }
            if (p1.getArgs()[i]<p2.getArgs()[i]) {
                return false;
            }
        }
    }
    return false;
}

bool operator==(POLYNOMIAL& p1, POLYNOMIAL& p2) {
    if (p1.getLevel()!=p2.getLevel()) {
        return false;
    }
    for (int i = p2.getLevel(); i>=0; i--) {
        if (p1.getArgs()[i]!=p2.getArgs()[i]) {
            return false;
        }
    }
    return true;
}

bool operator<=(POLYNOMIAL& p1, POLYNOMIAL& p2) {
    return (p1==p2 || p1<p2);
}

bool operator>=(POLYNOMIAL& p1, POLYNOMIAL& p2) {
    return (p1==p2 || p1>p2);
}

bool operator!=(POLYNOMIAL& p1, POLYNOMIAL& p2) {
    return !(p1==p2);
}

ostream& operator<<(ostream& out, const POLYNOMIAL& p) {
    out<<"( ";
    for (int i = 0; i<  p.level; i++) {
        out<<p.args[i]<<", ";
    }
    out<<p.args[p.level]<<" )";
    return out;
}

istream& operator>>(istream& in, POLYNOMIAL& p) {
    in >> p.level;
    for (int i = 0; i <= p.level; i++) {
        in >> p.args[i];
    }
    p.normalizePolynomial();
    return in;
}
